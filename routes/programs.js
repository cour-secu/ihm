var express = require('express');
var router = express.Router();

/* GET programs listing. */
router.get('/', function(req, res, next) {
  res.render('programs', {
    title: 'Programs'
  });
});

module.exports = router;
